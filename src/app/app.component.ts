import { Component } from '@angular/core';
import { AppService } from './Service/app.service';

import {item} from './Model/item';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
  providers: [AppService]
})
export class AppComponent {

  constructor(
    private appService: AppService
  ){}

  text = 'get data';

  clicked(event) {
    event.preventDefault();
    this.appService.getData()
      .subscribe(
        data => console.log(data), //Bind to view
        err => {
          // Log errors if any
          console.log(err);
        });
  }
}
