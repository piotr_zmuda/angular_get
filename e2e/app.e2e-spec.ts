import { ResearchPage } from './app.po';

describe('research App', function() {
  let page: ResearchPage;

  beforeEach(() => {
    page = new ResearchPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
